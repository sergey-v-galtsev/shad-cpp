# Hello World

В этом задании вам нужно познакомиться с системой тестирования и настроить
локальное окружение.

  - Прочитайте [SETUP.md](SETUP.md).
  - Ознакомьтесь с CMakeLists.txt в этой директории, разберитесь в том как собирается проект.
  - Посмотрите как написаны тесты и бенчмарки.
  - Запустите код в релизной сборке.
  - Прочитайте реализацию функции `Contains` в `string_util.cpp`, проверьте что тесты проходят.
  - Запустите код под ASAN-ом и TSAN-ом.
