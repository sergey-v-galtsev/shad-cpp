#pragma once

#include <mutex>

class SpinLock {
public:
    void lock() {
        mutex_.lock();
    }

    void unlock() {
        mutex_.unlock();
    }
private:
    std::mutex mutex_;
};
