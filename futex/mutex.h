#pragma once

#include <linux/futex.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>

#include <mutex>

// Atomically do the following:
//    if (*value == expected_value) {
//        sleep_on_address(value)
//    }
void FutexWait(int *value, int expected_value) {
    syscall(SYS_futex, value, FUTEX_WAIT_PRIVATE, expected_value);
}

// Wakeup 'count' threads sleeping on address of value
void FutexWake(int *value, int count) {
    syscall(SYS_futex, value, FUTEX_WAKE_PRIVATE, count);
}

class Mutex {
public:
    void lock() {
        mutex_.lock();
    }

    void unlock() {
        mutex_.unlock();
    }
private:
    std::mutex mutex_;
};
