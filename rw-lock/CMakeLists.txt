cmake_minimum_required(VERSION 2.8)
project(rw-lock)

if (TEST_SOLUTION)
    include_directories(../private/rw-lock/ok)
endif()

include(../common.cmake)

add_gtest(test_rwlock test.cpp)
add_benchmark(bench_rwlock run.cpp)
